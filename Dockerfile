FROM alpine:3

ARG CI_PROJECT_PATH
ARG BUILD_X86_64_MUSL_JOB_ID
ARG BUILD_AARCH64_MUSL_JOB_ID

RUN \
	case "$(uname -m)" in \
	x86_64) \
		wget "https://gitlab.com/${CI_PROJECT_PATH}/-/jobs/${BUILD_X86_64_MUSL_JOB_ID}/artifacts/raw/acme-ovh-dane" \
		;; \
	aarch64) \
		wget "https://gitlab.com/${CI_PROJECT_PATH}/-/jobs/${BUILD_AARCH64_MUSL_JOB_ID}/artifacts/raw/acme-ovh-dane" \
		;; \
	esac \
	&& mv acme-ovh-dane /bin \
	&& chmod 555 /bin/acme-ovh-dane

ENTRYPOINT acme-ovh-dane
