# ACME / OVH / DANE

[![GitLab](https://img.shields.io/gitlab/v/release/cl00e9ment/acme-ovh-dane?label=GitLab&logo=gitlab)](https://gitlab.com/Cl00e9ment/acme-ovh-dane)
[![Docker Hub](https://img.shields.io/docker/v/cl00e9ment/acme-ovh-dane?label=Docker%20Hub&logo=docker)](https://hub.docker.com/r/cl00e9ment/acme-ovh-dane)

- Generates and automatically renews an X.509 certificate with an ACME compatible authority (e.g. Let's Encrypt).
- Proves ownership of the domain with a DNS challenge.
- Adds a TLSA field to the DNS zone to enable [DANE](https://en.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities) 3 0 1 (DANE-EE).
- Certificate renewal is correctly handled by having the next TLSA field pushed to the DNS 2 TTL before the certificate change.

⚠️ Currently only works with [OVH](https://www.ovhcloud.com).

## How to get it?

### option 1: download the binary

Choose the version that correspond to your system [here](https://gitlab.com/Cl00e9ment/acme-ovh-dane/-/releases) and download it.

### option 2: download the Docker image

tags are:

- latest — the latest build
- \<major\> — the latest build having this major version
- \<major\>.\<minor\> — the latest build having this major and minor version
- \<version\> — a specific version

```bash
docker pull cl00e9ment/acme-ovh-dane:latest
```

### option 3: build it yourself

```bash
git clone https://gitlab.com/Cl00e9ment/acme-ovh-dane.git
cd acme-ovh-dane
cargo build --release
```

You can now find the binary is `target/release/acme-ovh-dane`.

## How to use it?

1. Run the program with the required [environment variables](#environment-variables) set.
2. The certificate will be automatically generated and renewed.
3. The data directory that you provided will contain the following:
   - `all/<unix_epoch>` directories containing all certificates that have been generated
   - `current` a symlink to the directory containing the current certificate
   - `next` a symlink (present around renewal dates) to the directory containing the next certificate that will soon become effective

## Example

Use case:

We want to generate and automatically renew the certificate for `mail.example.com`.
The DANE will be effective on the port 25 (SMTP) of this domain so our mail server can have a verified TLS connection with other mail severs supporting DANE (e.g. ProtonMail, Lavabit, etc...) 

### with the binary

```bash
DOMAIN=mail.example.com \
ACME_CONTACT_EMAIL=postmaster@example.com \
OVH_ENDPOINT=ovh_eu \
OVH_APPLICATION_KEY=XXXXXXXXXXXXXXXX \
OVH_APPLICATION_SECRET=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \
OVH_CONSUMER_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \
DANE_PORT=25 \
./acme-ovh-dane
```

### with the Docker image

```bash
docker run -it --rm -v data:/mnt/data \
-e DOMAIN=mail.example.com \
-e DATA_DIR=/mnt/data \
-e ACME_CONTACT_EMAIL=postmaster@example.com \
-e OVH_ENDPOINT=ovh_eu \
-e OVH_APPLICATION_KEY=XXXXXXXXXXXXXXXX \
-e OVH_APPLICATION_SECRET=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \
-e OVH_CONSUMER_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX \
-e DANE_PORT=25 \
cl00e9ment/acme-ovh-dane:latest
```

## Environment variables

| name                                                                                   | required | default value                                  |
|----------------------------------------------------------------------------------------|----------|------------------------------------------------|
| [DOMAIN](#domain)                                                                      | yes      |                                                |
| [DATA_DIR](#data_dir)                                                                  | no       | data                                           |
| [ACME_DIR_URL](#acme_dir_url)                                                          | no       | https://acme-v02.api.letsencrypt.org/directory |
| [ACME_CONTACT_EMAIL](#acme_contact_email)                                              | no       |                                                |
| [RENEW_X_DAYS_BEFORE_EXPIRATION](#renew_x_days_before_expiration)                      | no       | 30                                             |
| [OVH_ENDPOINT](#ovh_endpoint)                                                          | yes      |                                                |
| [OVH_APPLICATION_KEY](#ovh_application_key-ovh_application_secret-ovh_consumer_key)    | yes      |                                                |
| [OVH_APPLICATION_SECRET](#ovh_application_key-ovh_application_secret-ovh_consumer_key) | yes      |                                                |
| [OVH_CONSUMER_KEY](#ovh_application_key-ovh_application_secret-ovh_consumer_key)       | yes      |                                                |
| [TLSA_TTL_SEC](#tlsa_ttl_sec)                                                          | no       | 86400 (1 day)                                  |
| [DANE_PROTOCOL](#dane_protocol)                                                        | no       | TCP                                            |
| [DANE_PORT](#dane_port)                                                                | yes      |                                                |

### DOMAIN

The domain you want to issue the certificate for (wildcard domains are not supported yet).

### DATA_DIR

The directory that will contain the certificates.

### ACME_DIR_URL

The path to the ACME v2 directory that will be used to deliver the certificate.
By default, the Let's Encrypt directory is used.

### ACME_CONTACT_EMAIL

An optional contact email that is used to initialise the ACME account.

### RENEW_X_DAYS_BEFORE_EXPIRATION

The number of days before the certificate expiration when to renew the certificate.

### OVH_ENDPOINT

The OVH endpoint to use.
Here is a list of all valid endpoints:

- ovh-eu
- ovh-us
- ovh-ca
- kimsufi-eu
- kimsufi-ca
- soyoustart-eu
- soyoustart-ca

### OVH_APPLICATION_KEY, OVH_APPLICATION_SECRET, OVH_CONSUMER_KEY

The OVH credentials used to connect to the API to update the DNS.
To generate credentials for the `ovh-eu` endpoint, go [here](https://eu.api.ovh.com/createToken/) and fill the form:

- `Account ID or email address`: Your account ID (something like `cs123456-ovh`). Don't use your email address, it will not work.
- `Password`: Your account password.
- `Application name`: A name of your choice (e.g. `cert updater`).
- `Application description`: A description of your choice (e.g. `issues/updates certificate for mail.example.com`).
- `Validity`: Should be `Unlimited` or the program will stop working at some point.
- `Rights`: Add the following rights (replace `example.com` by your base name, don't include any subdomains):
  - `GET    /domain/zone/example.com/record*`
  - `DELETE /domain/zone/example.com/record*`
  - `POST   /domain/zone/example.com/record*`
  - `POST   /domain/zone/example.com/refresh`

### OVH_DNS_PROPAGATION_TIME_SEC

The number of seconds to wait before requesting the certificate after adding the ownership proof to the DNS.
[OVH says](https://docs.ovh.com/gb/en/domains/web_hosting_how_to_edit_my_dns_zone/#propagation-time_1) that propagation can take up to 24 hours, but in practice, 60 seconds seam to work great.
However, in production you may want to be extra careful.

### TLSA_TTL_SEC

The TTL (time to live) of the TLSA record in the DNS.
When renewing certificate, the program will add the new record to the DNS, wait 2 TTL and then apply the new certificate and remove the old record.

### DANE_PROTOCOL

The protocol on which DANE will be effective.
Valid protocols are:

- UDP
- TCP
- SCTP

### DANE_PORT

The port number on which DANE will be effective.
