use std::path::{Path, PathBuf};
use std::time::Duration;

use instant_acme::{Account, AuthorizationStatus, ChallengeType, Identifier, NewAccount, NewOrder, OrderStatus};
use log::info;
use rcgen::{CertificateParams, DistinguishedName, KeyPair};
use time::OffsetDateTime;
use tokio::fs;
use tokio::time::sleep;

use crate::{Config, dns, Result};
use crate::errors::ResultExt;
use crate::cert::Certificate;

async fn try_load_account<P: AsRef<Path>>(save_file_path: P) -> Option<Account> {
	let file_content = fs::read(save_file_path).await.ok()?;
	let creds = serde_json::from_slice(&file_content).ok()?;
	Account::from_credentials(creds).await.ok()
}

async fn create_account<P: AsRef<Path>>(config: &Config, save_file_path: P) -> Result<Account> {
	let contact = match &config.acme.contact_uri {
		Some(uri) => vec![uri.as_str()],
		None => Vec::with_capacity(0),
	};
	
	let (account, credentials) = Account::create(
		&NewAccount {
			contact: &contact,
			terms_of_service_agreed: true,
			only_return_existing: false,
		},
		&config.acme.dir_url,
		None,
	).await.chain_err(|| "failed to create account")?;

	let file_content = serde_json::to_vec_pretty(&credentials).unwrap();
	if let Err(e) = fs::write(save_file_path, file_content).await {
		log::warn!("failed to save account credentials: {}", e);
	}
	
	Ok(account)
}

pub async fn get_account(config: &Config) -> Result<Account> {
	let save_file_path = config.data_dir.join("account.json");
	
	match try_load_account(&save_file_path).await {
		Some(account) => Ok(account),
		None => create_account(config, &save_file_path).await,
	}
}

pub async fn request_cert(config: &Config, account: &Account, link_name: &str, clear_old_tlsa: bool) -> Result<Certificate> {
	// Create the ACME order based on the given domain names.
	let mut order = account
		.new_order(&NewOrder {
			identifiers: &[Identifier::Dns(config.domain.full.clone())],
		})
		.await?;

	// Check order status
	{
		let status = order.state().status;
		if status != OrderStatus::Pending {
			return Err(format!("expected ACME order status to be Pending, but got {:?}", status).into());
		}
	}
	
	// Obtain the authorization to proceed for ACME challenge
	let authorization = {
		let mut authorizations = order.authorizations().await?;
		if authorizations.len() != 1 {
			return Err(format!("expected to get 1 authorization to proceed for ACME challenge, but got {}", authorizations.len()).into());
		}
		authorizations.remove(0)
	};

	if authorization.status != AuthorizationStatus::Pending {
		return Err(format!("expected authorization to proceed for ACME challenge, to be Pending, but got {:#?}", authorization.status).into());
	}

	// We'll use the DNS challenge
	let challenge = authorization
		.challenges
		.iter()
		.find(|c| c.r#type == ChallengeType::Dns01)
		.chain_err(|| "authorization for ACME challenge do not allow for dns01 challenge")?;

	// Prepare the challenge response
	dns::push_acme_proof(config, &order.key_authorization(challenge).dns_value()).await?;

	// Let the server know we're ready to accept the challenge
	order.set_challenge_ready(&challenge.url).await?;

	// Exponentially back off until the order becomes ready or invalid
	{
		let mut tries: u8 = 0;
		let mut delay = Duration::from_millis(250);
		loop {
			let status = order.refresh().await?.status;
			tries += 1;

			if status == OrderStatus::Ready {
				info!("ACME order is {:?}", status);
				break;
			} else if status == OrderStatus::Invalid {
				return Err(format!("ACME order is {:?}. DNS propagation time may be too short", status).into());
			}

			if tries == 10 {
				return Err(format!("max tries exceeded. ACME order is {:?}", status).into());
			}

			delay *= 2;
			info!("ACME order is {:?}, waiting {}s", status, delay.as_secs_f32());
			sleep(delay).await;
		}
	}

	// If the order is ready, we can provision the certificate
	// Use the rcgen library to create a Certificate Signing Request.
	let mut params = CertificateParams::new(vec![config.domain.full.clone()])?;
	params.distinguished_name = DistinguishedName::new();
	let private_key = KeyPair::generate()?;
	let csr = params.serialize_request(&private_key)?;

	// Finalize the order
	order.finalize(csr.der()).await?;
	let cert_chain_pem = loop {
		match order.certificate().await? {
			Some(cert_chain_pem) => break cert_chain_pem,
			None => {
				info!("waiting for certificate signing request to complete...");
				sleep(Duration::from_secs(1)).await
			},
		}
	};
	
	let cert = Certificate::from_pem(&cert_chain_pem).chain_err(|| "failed to parse certificate")?;

	info!("certificate generated");

	dns::clear_acme_push_tlsa(config, &cert, clear_old_tlsa).await?;
	
	let dir_rel_path = PathBuf::from("all").join(OffsetDateTime::now_utc().unix_timestamp().to_string());
	let dir_abs_path = config.data_dir.join(&dir_rel_path);
	
	fs::create_dir_all(&dir_abs_path).await?;
	fs::write(dir_abs_path.join("certificate.crt"), cert_chain_pem).await?;
	fs::write(dir_abs_path.join("private.key"), private_key.serialize_pem()).await?;
	fs::symlink(dir_rel_path, config.data_dir.join(link_name)).await?;
	
	info!("certificate saved");
	
	Ok(cert)
}
