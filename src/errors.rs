use error_chain::error_chain;

error_chain! {
	foreign_links {
		Io(std::io::Error);
		InstantAcme(instant_acme::Error);
		Num(std::num::ParseIntError);
		Pem(pem::PemError);
		Rcgen(rcgen::Error);
		Time(time::error::ComponentRange);
	}
}
