use std::str::FromStr;
use pem::Pem;
use rcgen::CertificateParams;
use crate::errors::Result;

pub struct Certificate {
	params: CertificateParams,
	content: Vec<u8>,
}

impl Certificate {
	pub fn from_pem(pem_str: &str) -> Result<Self> {
		Ok(Certificate {
			params: CertificateParams::from_ca_cert_pem(pem_str)?,
			content: Pem::from_str(pem_str)?.into_contents(),
		})
	}
	
	pub fn params(&self) -> &CertificateParams {
		&self.params
	}
	
	pub fn content(&self) -> &[u8] {
		&self.content
	}
}
