use std::{env, result};
use std::env::VarError;
use VarError::{NotPresent, NotUnicode};

use crate::env::EnvVal::{ValErr, ValNone, ValSome};
use crate::errors::{Error, Result};

enum EnvVal<T> {
	ValSome(T),
	ValNone,
	ValErr(Error),
}

pub struct Env<'a, T> {
	key: &'a str,
	val: EnvVal<T>,
}

impl Env<'_, String> {
	pub fn get(key: &str) -> Env<String> {
		let val = match env::var(key) {
			Ok(str) => ValSome(str),
			Err(e) => match e {
				NotPresent => ValNone,
				NotUnicode(_) => ValErr(format!("value of {} environment variable isn't valid Unicode", key).into()),
			},
		};
		Env { key, val }
	}
}

impl <'a, T> Env<'a, T> {
	pub fn parse<R, E>(self, parser: fn (T) -> result::Result<R, E>) -> Env<'a, R>
		where E: std::error::Error + Send + 'static
	{
		let key = self.key;
		let val = match self.val {
			ValSome(v) => match parser(v) {
				Ok(v) => ValSome(v),
				Err(e) => ValErr(Error::with_chain(e, format!("failed to parse {} environment variable", key))),
			},
			ValNone => ValNone,
			ValErr(e) => ValErr(e),
		};
		Env { key, val }
	}

	pub fn map<R>(self, mapper: fn (T) -> R) -> Env<'a, R> {
		let key = self.key;
		let val = match self.val {
			ValSome(v) => ValSome(mapper(v)),
			ValNone => ValNone,
			ValErr(e) => ValErr(e),
		};
		Env { key, val }
	}

	pub fn or(self, default: T) -> Result<T> {
		match self.val {
			ValSome(v) => Ok(v),
			ValNone => Ok(default),
			ValErr(e) => Err(e),
		}
	}

	pub fn or_else(self, default: fn() -> T) -> Result<T> {
		match self.val {
			ValSome(v) => Ok(v),
			ValNone => Ok(default()),
			ValErr(e) => Err(e),
		}
	}

	pub fn optional(self) -> Result<Option<T>> {
		match self.val {
			ValSome(v) => Ok(Some(v)),
			ValNone => Ok(None),
			ValErr(e) => Err(e),
		}
	}

	pub fn required(self) -> Result<T> {
		match self.val {
			ValSome(v) => Ok(v),
			ValNone => Err(format!("{} environment variable isn't set", self.key).into()),
			ValErr(e) => Err(e),
		}
	}
}

impl <'a> Env<'a, i64> {
	pub fn expect_gt(self, other: i64) -> Self {
		let key = self.key;
		let val = match self.val {
			ValSome(x) if x <= other => ValErr(format!("expected {} environment variable to be greater than {}", key, other).into()),
			default => default,
		};
		Env { key, val }
	}

	pub fn expect_lte(self, other: i64) -> Self {
		let key = self.key;
		let val = match self.val {
			ValSome(x) if x > other => ValErr(format!("expected {} environment variable to be less than or equal to {}", key, other).into()),
			default => default,
		};
		Env { key, val }
	}
}
