use std::fmt::{Display, Formatter};
use std::path::{PathBuf};
use std::str::FromStr;

use addr::parse_domain_name;
use error_chain::bail;
use ovh::client::OvhClient;
use time::Duration;
use tokio::{fs, io};

use crate::config::Protocol::{SCTP, TCP, UDP};
use crate::env::Env;
use crate::errors::{Error, Result};
use crate::ResultExt;

pub enum Protocol {
	UDP,
	TCP,
	SCTP,
}

impl FromStr for Protocol {
	type Err = Error;

	fn from_str(str: &str) -> Result<Self> {
		match str.to_ascii_uppercase().as_str() {
			"UDP" => Ok(UDP),
			"TCP" => Ok(TCP),
			"SCTP" => Ok(SCTP),
			_ => Err(format!("\"{}\" isn't a valid protocol, expected either UDP, TCP or SCTP", str).into()),
		}
	}
}

impl Display for Protocol {
	fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
		f.write_str(match self {
			UDP => "udp",
			TCP => "tcp",
			SCTP => "sctp",
		})
	}
}

pub struct Domain {
	pub full: String,
	pub root: String,
	pub sub_domain: Option<String>,
}

pub struct Acme {
	pub dir_url: String,
	pub contact_uri: Option<String>,
}

pub struct Dane {
	pub port: u16,
	pub protocol: Protocol,
}

pub struct Config {
	pub domain: Domain,
	pub acme: Acme,
	pub dane: Dane,
	pub ovh_client: OvhClient,
	pub dns_propagation_time: Duration,
	pub data_dir: PathBuf,
	pub renew_before: Duration,
	pub tlsa_ttl: Duration,
}

impl Config {
	pub async fn from_env() -> Result<Config> {
		let domain = {
			let full = Env::get("DOMAIN").required()?;
			let parsed_domain = parse_domain_name(&full).map_err(|e| Error::from(e.to_string()).chain_err(|| "failed to parse DOMAIN"))?;
			let root = parsed_domain.root().ok_or("DOMAIN is invalid (failed to get root)")?.to_string();
			let sub_domain = parsed_domain.prefix().map(|str| str.to_string());
			Domain { full, root, sub_domain }
		};

		let acme = {
			let dir_url = Env::get("ACME_DIR_URL").or_else(|| String::from("https://acme-v02.api.letsencrypt.org/directory"))?;
			let contact_uri = Env::get("ACME_CONTACT_EMAIL").optional()?.map(|email| format!("mailto:{}", email));
			Acme { dir_url, contact_uri }
		};

		let dane = {
			let port = Env::get("DANE_PORT").parse(|str| str.parse::<u16>()).required()?;
			let protocol = Env::get("DANE_PROTOCOL").parse(|str| Protocol::from_str(&str)).or(TCP)?;
			Dane { port, protocol }
		};

		let dns_propagation_time = Env::get("OVH_DNS_PROPAGATION_TIME_SEC")
			.parse(|str| str.parse::<i64>())
			.expect_gt(0)
			.or(60)
			.map(Duration::seconds)?;

		let data_dir = Env::get("DATA_DIR")
			.map(PathBuf::from)
			.or_else(|| PathBuf::from("data"))?
			.canonicalize()
			.map_err(|e| Error::from(e).chain_err(|| "invalid path provided as DATA_DIR"))?;

		if let Err(e) = fs::create_dir(&data_dir).await {
			if e.kind() != io::ErrorKind::AlreadyExists {
				return Err(Error::with_chain(e, "failed to create data dir"));
			}
		}

		let renew_before = Env::get("RENEW_X_DAYS_BEFORE_EXPIRATION")
			.parse(|str| str.parse::<i64>())
			.expect_gt(0)
			.or(30)
			.map(Duration::days)?;

		let tlsa_ttl = Env::get("TLSA_TTL_SEC")
			.parse(|str| str.parse::<i64>())
			.expect_gt(0)
			.expect_lte(i32::MAX as i64)
			.or(86400)
			.map(Duration::seconds)?;

		if tlsa_ttl * 2 > renew_before {
			bail!("RENEW_X_DAYS_BEFORE_EXPIRATION ✕ \"secs in a day\" should be at least 2 time greater than TLSA_TTL_SEC");
		}

		let ovh_client = OvhClient::new(
			&Env::get("OVH_ENDPOINT").required()?,
			&Env::get("OVH_APPLICATION_KEY").required()?,
			&Env::get("OVH_APPLICATION_SECRET").required()?,
			&Env::get("OVH_CONSUMER_KEY").required()?,
		).await.map_err(|e| Error::from(e.to_string())).chain_err(|| "failed to create OVH client")?;

		Ok(Config {
			domain,
			acme,
			dane,
			ovh_client,
			dns_propagation_time,
			data_dir,
			renew_before,
			tlsa_ttl,
		})
	}
}
