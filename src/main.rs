use std::io::ErrorKind::NotFound;

use log::{error, info, LevelFilter};
use simplelog::{ColorChoice, ConfigBuilder, TerminalMode, TermLogger, format_description};
use time::{Duration, OffsetDateTime};
use tokio::fs;

use crate::cert::Certificate;
use crate::config::Config;
use crate::errors::{Error, Result, ResultExt};

mod acme;
mod config;
mod dns;
mod env;
mod errors;
mod cert;

#[tokio::main]
async fn main() {
	let logger_conf = ConfigBuilder::new()
		.add_filter_allow_str("acme_ovh_dane")
		.set_time_format_custom(format_description!("[[[year]-[month]-[day] [hour]:[minute]:[second]]"))
		.build();

	TermLogger::init(LevelFilter::Info, logger_conf, TerminalMode::Mixed, ColorChoice::Auto).unwrap();

	if let Err(e) = run().await {
		error!("{}", e.iter()
			.map(|e| e.to_string())
			.collect::<Vec<String>>()
			.join(": ")
		);
	}
}

async fn get_cert(config: &Config, name: &str) -> Result<Option<Certificate>> {
	match fs::read_to_string(config.data_dir.join(name).join("certificate.crt")).await {
		Ok(content) => Certificate::from_pem(&content)
			.map_err(|e| Error::from(e.to_string()))
			.map(Some),
		Err(e) if e.kind() == NotFound => Ok(None),
		Err(e) => Err(e.into()),
	}
}

async fn run() -> Result<()> {
	info!("setting up everything");
	let config = Config::from_env().await?;

	let account = acme::get_account(&config)
		.await
		.chain_err(|| "failed to initialize the ACME account")?;

	// ******

	let current_cert = get_cert(&config, "current")
		.await
		.chain_err(|| "failed to load \"current/certificate.crt\"")?;

	let mut current_cert = if let Some(cert) = current_cert {
		cert
	} else {
		info!("certificate not found, generating a new one");
		acme::request_cert(&config, &account, "current", true)
			.await
			.chain_err(|| "failed to generate certificate")?
	};

	// ******

	loop {
		let renewal_date = current_cert.params().not_after - config.renew_before;
		let until_renewal = renewal_date - OffsetDateTime::now_utc();

		if until_renewal.is_positive() {
			info!("waiting until the renewal date: {}", renewal_date);
			sleep(until_renewal).await;
		}

		// ******

		let next_cert = get_cert(&config, "next")
			.await
			.chain_err(|| "failed to load \"next/certificate.crt\"")?;

		let next_cert = if let Some(cert) = next_cert {
			cert
		} else {
			info!("generating the next certificate");
			acme::request_cert(&config, &account, "next", false)
				.await
				.chain_err(|| "failed to generate certificate")?
		};

		// ******

		let next_cert_application_date = get_cert_delivery_date(&config, "next").await
			.chain_err(|| "can't recover certificate delivery date using file name")?
			+ config.tlsa_ttl * 2;

		let until_next_cert: Duration = next_cert_application_date - OffsetDateTime::now_utc();

		if until_next_cert.is_positive() {
			info!("waiting until 2 TLSA TTL have passed before applying the next certificate: {}", next_cert_application_date);
			sleep(until_next_cert).await;
		}

		// ******

		info!("applying the next certificate");
		current_cert = next_cert;
		fs::rename(config.data_dir.join("next"), config.data_dir.join("current"))
			.await
			.chain_err(|| "failed to change where the \"current\" directory points")?;

		info!("cleaning old TLSA records");
		dns::keep_only_latest_tlsa(&config, &current_cert).await?;
	}
}

async fn get_cert_delivery_date(config: &Config, name: &str) -> Result<OffsetDateTime> {
	let epoch: i64 = fs::read_link(config.data_dir.join(name))
		.await?
		.file_name().unwrap()
		.to_str().ok_or("data directory contains files with invalid unicode")?
		.parse()?;

	Ok(OffsetDateTime::from_unix_timestamp(epoch)?)
}

/// little helper to do an async tokio::time::sleep with a time::duration::Duration
pub async fn sleep(duration: Duration) {
	if duration.is_zero() {
		return;
	}

	assert!(duration.is_positive());

	let duration =
		  core::time::Duration::from_secs(duration.whole_seconds() as u64)
		+ core::time::Duration::from_nanos(duration.subsec_nanoseconds() as u64);

	tokio::time::sleep(duration).await
}
