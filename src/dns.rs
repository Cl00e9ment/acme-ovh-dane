use std::fmt::Write;

use futures_util::future;
use log::info;
use ovh::dns_record::{DnsRecordType, OvhDnsRecord};
use ovh::dns_record::DnsRecordType::{TLSA, TXT};
use ring::digest;
use ring::digest::digest;
use crate::{sleep, Config, Error, Result};
use crate::cert::Certificate;

macro_rules! chain_ovh_err {
	($msg:expr) => {
		|e| Error::from(e.to_string()).chain_err(|| $msg)
	};
}

pub async fn push_acme_proof(config: &Config, proof: &str) -> Result<()> {
	clear_acme(config).await?;

	info!("adding the proof to the DNS");
	OvhDnsRecord::create(&config.ovh_client, Some(&acme_sub_domain(config)), &config.domain.root, TXT, Some(3600), proof, false)
		.await
		.map_err(chain_ovh_err!("failed to create proof TXT record"))?;

	refresh_zone(config).await?;

	info!("waiting for DNS propagation");
	sleep(config.dns_propagation_time).await;

	Ok(())
}

pub async fn clear_acme_push_tlsa(config: &Config, cert: &Certificate, clear_old_tlsa: bool) -> Result<()> {
	clear_acme(config).await?;

	let fingerprint = hash_certificate(cert);
	let target = format!("3 0 1 {}", fingerprint);
	let sub_domain = tlsa_sub_domain(config);
	let ttl = config.tlsa_ttl.whole_seconds() as i32;

	if clear_old_tlsa {
		clear_tlsa(config).await?;
	}

	info!("adding the TLSA field to the DNS");
	OvhDnsRecord::create(&config.ovh_client, Some(&sub_domain), &config.domain.root, TLSA, Some(ttl), &target, false)
		.await
		.map_err(chain_ovh_err!("failed to create TLSA record"))?;

	refresh_zone(config).await?;

	Ok(())
}

pub async fn keep_only_latest_tlsa(config: &Config, cert: &Certificate) -> Result<()> {
	let fingerprint = hash_certificate(cert);
	let target = format!("3 0 1 {}", fingerprint);
	let sub_domain = tlsa_sub_domain(config);
	let ttl = config.tlsa_ttl.whole_seconds() as i32;

	let current_fields = OvhDnsRecord::list_filtered(&config.ovh_client, &config.domain.root, Some(TLSA), Some(&sub_domain))
		.await
		.map_err(chain_ovh_err!("failed to list current TLSA fields"))?;

	let mut to_delete = Vec::new();
	let mut found = false;

	for field in current_fields {
		if field.target == target {
			found = true;
		} else {
			to_delete.push(OvhDnsRecord::delete(&config.ovh_client, &config.domain.root, field.id, false));
		}
	}

	future::join_all(to_delete)
		.await
		.into_iter()
		.collect::<ovh::client::Result<Vec<()>>>()
		.map_err(chain_ovh_err!("failed to delete old TLSA records"))?;

	if !found {
		log::warn!("TLSA field not found in DNS, adding it");
		OvhDnsRecord::create(&config.ovh_client, Some(&sub_domain), &config.domain.root, TLSA, Some(ttl), &target, false)
			.await
			.map_err(chain_ovh_err!("failed to create TLSA record"))?;
	}

	refresh_zone(config).await?;

	Ok(())
}

async fn clear_acme(config: &Config) -> Result<()> {
	clear_old_records(config, "TXT proofs", TXT, &acme_sub_domain(config)).await
}

async fn clear_tlsa(config: &Config) -> Result<()> {
	clear_old_records(config, "TLSA", TLSA, &tlsa_sub_domain(config)).await
}

async fn clear_old_records(config: &Config, records_name: &str, record_type: DnsRecordType, sub_domain: &str) -> Result<()> {
	info!("clearing old {} records from the DNS", records_name);

	let deletion = OvhDnsRecord::list_ids_filtered(&config.ovh_client, &config.domain.root, Some(record_type), Some(sub_domain))
		.await
		.map_err(chain_ovh_err!(format!("failed to get ID of old {} records", records_name)))?
		.into_iter()
		.map(|id| OvhDnsRecord::delete(&config.ovh_client, &config.domain.root, id, false));

	future::join_all(deletion)
		.await
		.into_iter()
		.collect::<ovh::client::Result<Vec<()>>>()
		.map_err(chain_ovh_err!(format!("failed to delete old {} records", records_name)))?;

	Ok(())
}

async fn refresh_zone(config: &Config) -> Result<()> {
	info!("refreshing the DNS zone");
	OvhDnsRecord::refresh_zone(&config.ovh_client, &config.domain.root)
		.await
		.map_err(|e| Error::from(e.to_string()).chain_err(|| "failed to refresh the DNS zone"))
}

fn hash_certificate(cert: &Certificate) -> String {
	let hash = digest(&digest::SHA256, cert.content());

	let mut res = String::with_capacity(64);
	for byte in hash.as_ref() {
		write!(&mut res, "{:02x}", byte).unwrap();
	}
	res
}

fn acme_sub_domain(config: &Config) -> String {
	extend_sub_domain(config, "_acme-challenge")
}

fn tlsa_sub_domain(config: &Config) -> String {
	extend_sub_domain(config, &format!("_{}._{}", config.dane.port, config.dane.protocol))
}

fn extend_sub_domain(config: &Config, extension: &str) -> String {
	match &config.domain.sub_domain {
		Some(sub_domain) => format!("{}.{}", extension, sub_domain),
		None => String::from(extension),
	}
}
